import { Component, input, OnInit} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Employee } from '../interface/home.model';
import { EmployeeService } from '../services/userServices.service';
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'


@Component({
  selector: 'app-home',
  standalone: true,
  imports: [FormsModule, CommonModule, MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit {
 employees: Employee[] = []
 isPassHidden: boolean = true
 uname: string = ''
 password: string = ''
 isWrongPass: boolean = false

constructor(private employeeService: EmployeeService, private router: Router) {};

 ngOnInit(): void {}

 changePassView():void {
  this.isPassHidden = !this.isPassHidden
 }
 
 handleLogin():void {
  if(!this.uname) {
    this.isWrongPass = true
    return
  } else if(!this.password) {
    this.isWrongPass = true
    return
  }
  this.employeeService.getEmployees().subscribe(res => {
    this.employees = res
    let isUname = this.employees.find(el => el.username == this.uname)
    if(!isUname) {
      this.isWrongPass = true
      return
    } else {
      if(this.password !== isUname.password) {
        this.isWrongPass = true
        return
      } else {
        localStorage.setItem('login', 'true')
        this.router.navigate(['/list'])
      }
    }
  })
 }
}
