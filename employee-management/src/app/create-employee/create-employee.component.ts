import { Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import { MatIconModule} from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button'
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Employee } from '../interface/home.model';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgForm, NgModel,} from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { Router } from '@angular/router';
import { EmployeeService } from '../services/userServices.service';


@Component({
  selector: 'app-create-employee',
  standalone: true,
  imports: [MatSelectModule, MatDatepickerModule, CommonModule, FormsModule, MatInputModule, MatIconModule, MatButtonModule, MatFormFieldModule],
  templateUrl: './create-employee.component.html',
  styleUrl: './create-employee.component.scss'
})
export class CreateEmployeeComponent implements OnInit {

  @ViewChild('userForm') userForm!:NgForm
  @ViewChild('username') username!: NgModel;
  @ViewChild('firstname') firstname!: NgModel;
  @ViewChild('lastname') lastname!: NgModel;
  @ViewChild('email') email!: NgModel;
  @ViewChild('birthdate') birthdate!: NgModel;
  @ViewChild('salary') salary!: NgModel;
  @ViewChild('status') status!: NgModel;
  @ViewChild('group') group!: NgModel;
  @ViewChild('description') description!: NgModel;

  constructor(private router: Router, private employeeService: EmployeeService) {}

  statuses: string[]=[
    'Active', 'Inactive'
  ]

  groupTypes: {type: string} [] = [
    {type: "IT"},
    {type: "Marketing"},
    {type: "HR"}, 
    {type: "Operations"},
    {type: "Finance"},
    {type: "Support"},
    {type: "Sales"},
    {type: "Legal"},
    {type: "Logistics"},
    {type: "Engineering"}
  ]

  groupFiltered: {type: string}[] = []

  employee: Employee = {
    username: '',
    firstName: '',
    lastName: '',
    birthDate: null,
    basicSalary: null,
    description: '',
    email: '',
    group: '',
    password: '',
    status: ''
  }
  today: Date = new Date()
  groupKeyword: string = ''
  isModalError: boolean = false
  isModalSuccess: boolean = false

  ngOnInit(): void {
    if(typeof window! !== undefined && window!.localStorage) {
      if(!localStorage.getItem('login')) {
        this.router.navigate(['/home'])
        return
      }
    }
    this.filterGroups()
  }

  back():void {
    this.router.navigate(['/list'])
  }

  saveEmployee():void {
    let isValid:boolean = true

    for(let key in this.employee) {
      if(!this.employee[key as keyof Employee]) {
        if(key == 'password') {
          continue;
        }
        isValid = false
      }
    }
    if(!isValid) {
      this.username.control.markAsTouched()
      this.firstname.control.markAsTouched()
      this.lastname.control.markAsTouched()
      this.email.control.markAsTouched()
      this.birthdate.control.markAsTouched()
      this.salary.control.markAsTouched()
      this.status.control.markAsTouched()
      this.group.control.markAsTouched()
      this.description.control.markAsTouched()
      this.toggleModal(false)
    } else {
      this.employeeService.addData(this.employee)
      this.toggleModal(true)
        setTimeout(() => {
          this.router.navigate(['/list'])
        }, 2000)
    }
  }

  toggleModal(type: boolean):void {
    if(!type) this.isModalError = !this.isModalError
    else this.isModalSuccess = !this.isModalSuccess 
  }

  resetSearch():void {
    this.groupKeyword = ''
    this.groupFiltered = this.groupTypes
  }

  filterGroups():void {
    if(!this.groupKeyword) {
      this.groupFiltered = this.groupTypes
    } else {
      this.groupFiltered = this.groupTypes.filter(el => el.type.toLowerCase().includes(this.groupKeyword.toLowerCase()))
    }
  }
}
