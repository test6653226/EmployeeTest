import { Component, OnInit} from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { Employee } from '../interface/home.model';
import { EmployeeService } from '../services/userServices.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { listPagination } from '../interface/list.model';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button'
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-list',
  standalone: true,
  imports: [MatButtonModule, MatIconModule,MatSelectModule, MatCardModule,FormsModule, CommonModule, MatPaginatorModule, MatInputModule, MatFormFieldModule],
  templateUrl: './employee-list.component.html',
  styleUrl: './employee-list.component.scss'
})
export class EmployeeListComponent implements OnInit {

  employees: Employee[] = []
  pageSize: number = 10
  pageSizeOption: number[] = [5,10,25,100]
  searchKeyword: string = ''
  lengthData: number = 0
  actualPaginationSet: listPagination = {previousPageIndex: undefined, pageSize: this.pageSize, pageIndex: 0, length: undefined }
  groupTypes: {type: string} [] = [
    {type: "IT"},
    {type: "Marketing"},
    {type: "HR"}, 
    {type: "Operations"},
    {type: "Finance"},
    {type: "Support"},
    {type: "Sales"},
    {type: "Legal"},
    {type: "Logistics"},
    {type: "Engineering"}
  ]
  groupSort: string = ''


  constructor(private employeeService: EmployeeService, private router: Router) {}

  ngOnInit(): void {
    let isData: string | null = null
    let parsedData: {
      previousPageIndex: number,
      pageSize: number,
      pageIndex: number,
      length: number,
      searchKeyword: string,
      groupSort: string
    } | null = null

      if(typeof window !== undefined && window.localStorage) {
        if(!localStorage.getItem('login')) {
          this.router.navigate(['/home'])
          return
        }
        isData = localStorage.getItem('listInfo')
        parsedData = isData ? JSON.parse(isData) : undefined
      }

      if(!parsedData) {
        this.handlePageEvent(this.actualPaginationSet)
      } else {
        let {groupSort, length, pageIndex, pageSize, previousPageIndex, searchKeyword} = parsedData
        this.actualPaginationSet.length = length
        this.actualPaginationSet.pageIndex = pageIndex
        this.actualPaginationSet.pageSize = pageSize
        this.actualPaginationSet.previousPageIndex = previousPageIndex
        this.searchKeyword = searchKeyword
        this.groupSort = groupSort
        this.handlePageEvent(this.actualPaginationSet)
        localStorage.removeItem('listInfo')
      }
  }

  handleSearch():void {
    this.handlePageEvent(this.actualPaginationSet)
  }

  handlePageEvent(event :listPagination):void {
    this.actualPaginationSet = event
    this.pageSize = event.pageSize
    this.employeeService.getEmployees().subscribe(res => {
      this.employees = res
      this.lengthData = res.length
      let groups : string[] = []
      res.forEach(el => {
        if(!groups.includes(el.group)) {
          groups.push(el.group)
        }
      })
      if(this.searchKeyword) {
        this.employees = this.employees.filter(el => el.firstName.toLowerCase().includes(this.searchKeyword.toLowerCase()) || el.lastName.toLowerCase().includes(this.searchKeyword.toLowerCase()) || `${el.firstName} ${el.lastName}`.toLowerCase().includes(this.searchKeyword.toLowerCase()))
        this.lengthData = this.employees.length
      }
      if(this.groupSort) {
        this.employees = this.employees.filter(el => el.group.toLowerCase() == this.groupSort.toLowerCase())
        this.lengthData = this.employees.length
      }
      let startIndex = (event.pageIndex) * event.pageSize
      let endIndex = startIndex + event.pageSize
      this.employees = this.employees.slice(startIndex, endIndex)
    })
  }

  goCreateEmployee():void {
    this.router.navigate(['/addEmployee'])
    let pageInfo = {
      previousPageIndex: this.actualPaginationSet.previousPageIndex,
      pageSize: this.actualPaginationSet.pageSize,
      pageIndex: this.actualPaginationSet.pageIndex,
      length: this.lengthData,
      searchKeyword: this.searchKeyword,
      groupSort: this.groupSort
    }
    localStorage.setItem('listInfo', JSON.stringify(pageInfo))
  }

  goToDetail(username: string):void {
    this.router.navigate(['/detailEmployee'], {queryParams: {username}})
    //untuk state sementara, harusnya pakai state management
    //tapi karena belum pernah memakai di angular sebelumnya,
    //akan makan waktu lama untuk explore, untuk sekarang pakai local dulu
    let pageInfo = {
      previousPageIndex: this.actualPaginationSet.previousPageIndex,
      pageSize: this.actualPaginationSet.pageSize,
      pageIndex: this.actualPaginationSet.pageIndex,
      length: this.lengthData,
      searchKeyword: this.searchKeyword,
      groupSort: this.groupSort
    }
    localStorage.setItem('listInfo', JSON.stringify(pageInfo))
  }
}
