import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, switchMap } from 'rxjs';
import { Employee } from '../interface/home.model';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {
  private apiUrl = 'assets/dummy/userData.json';

  constructor(private http: HttpClient) {}

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.apiUrl);
  }
  addData(newData: Employee): void {
    let data: Employee[] = [];
    this.getEmployees().subscribe(
      res => {
        data = res;
        const updatedData = [...data, newData];
        this.http.put(this.apiUrl, updatedData).subscribe({
          next: (value) => {
            console.log(value, 'Data updated successfully');
          },
          error: (error) => {
            console.error('Failed to update data:', error);
          },
          complete: () => {
            console.log('HTTP PUT request complete');
          }
        });
      },
      (error) => {
        console.error('Failed to fetch employees:', error);
      }
    );
  }
}