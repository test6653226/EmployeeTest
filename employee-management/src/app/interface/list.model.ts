export interface listPagination {
    previousPageIndex?: number | undefined,
    pageSize: number, 
    pageIndex: number,
    length: number | undefined
}