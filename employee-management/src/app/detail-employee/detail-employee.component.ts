import { Component, OnInit} from '@angular/core';
import { MatButtonModule } from '@angular/material/button'
import { Router, ActivatedRoute} from '@angular/router';
import { Employee } from '../interface/home.model';
import { EmployeeService } from '../services/userServices.service';

@Component({
  selector: 'app-detail-employee',
  standalone: true,
  imports: [MatButtonModule],
  templateUrl: './detail-employee.component.html',
  styleUrl: './detail-employee.component.scss'
})
export class DetailEmployeeComponent implements OnInit {
  
  employee: Employee | undefined = undefined
  employeeEdited: any = undefined

  constructor(private router:Router, private route:ActivatedRoute, private employeeService: EmployeeService){}

  ngOnInit(): void {
      if(typeof window! !== undefined && window!.localStorage) {
        if(!localStorage.getItem('login')) {
          this.router.navigate(['/home'])
          return
        }
      }
      let username = this.route.snapshot.queryParams['username']
      this.employeeService.getEmployees().subscribe(res => {
        let datas:Employee[] = res
        this.employee = datas.find(el => el.username == username)
        if(this.employee) {
          let {basicSalary, birthDate, description, 
            email, firstName, group, lastName, 
            status, username} = this.employee
            let birtdateFormated = this.convertBirthDate(birthDate!)
            let salaryFormated = this.convertRupiah(basicSalary!)
            this.employeeEdited = {
              description,
              email,
              firstName,
              lastName,
              group,
              status,
              username,
              birthDate: birtdateFormated,
              basicSalary: salaryFormated
            }
        }
      })
  }

  convertRupiah(data:number):string {
    return data.toLocaleString('id-ID',{style:'currency', currency:'IDR'})
  }

  convertBirthDate(date: Date):string {
    let data = new Date(date)
    let monthList = [
      {value: 0, desc: 'January'},
      {value: 1, desc: 'February'},
      {value: 2, desc: 'March'},
      {value: 3, desc: 'April'},
      {value: 4, desc: 'Mei'},
      {value: 5, desc: 'June'},
      {value: 6, desc: 'July'},
      {value: 7, desc: 'August'},
      {value: 8, desc: 'September'},
      {value: 9, desc: 'October'},
      {value: 10, desc: 'November'},
      {value: 11, desc: 'December'},
    ]
    let dayList = [
      {value: 0, desc: "Sunday"},
      {value: 1, desc: "Monday"},
      {value: 2, desc: "Tuesday"},
      {value: 3, desc: "Wednesday"},
      {value: 4, desc: "Thursday"},
      {value: 5, desc: "Friday"},
      {value: 6, desc: "Saturday"}
    ]
    return `${dayList.find(el => el.value == data.getDay())?.desc}, ${data.getDate()} ${monthList.find(el => el.value == data.getMonth())?.desc} ${data.getFullYear()}`
  }

  back():void {
    this.router.navigate(['/list'])
  }
}
