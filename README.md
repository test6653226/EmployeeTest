# EmployeeTest - Muhammad Ihsan Erdiansyah

## Note
Mungkin jadi pertimbangan penilaian:
- Belum pernah membuat Angular from scratch
- Saat di perusahaan sebelumnya hanya menggunakan Angular ver. 1
- Belum pernah menggunakan Typescript di Angular

Project ini murni explorasi dalam waktu 2 hari ini, mudah-mudahan menjadi pertimbangan penilaian

## How To Run

pindah path ke EmployeeTest/employee-management dengan cd employee-management/

run 'npm install'

run 'npm start' / 'ng serve'

Hosted at http://localhost:4200/

Username: User1
Password: User1

